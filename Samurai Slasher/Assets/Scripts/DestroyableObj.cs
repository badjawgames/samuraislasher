﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableObj : MonoBehaviour
{
    void OnTriggerEnter2D() 
    {
        Destroy(this.gameObject);
    }
}
