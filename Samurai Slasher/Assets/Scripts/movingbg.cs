﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movingbg : MonoBehaviour
{
	private float speed = 0.2f;
	Vector2 bgpos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        bgpos = new Vector2 (Time.time*speed, 0);
		GetComponent<Renderer> ().material.mainTextureOffset = bgpos;
    }
}
