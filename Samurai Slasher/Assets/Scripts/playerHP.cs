﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerHP : MonoBehaviour
{
    public float max_health = 20f;
    public float cur_health = 0f;
    public GameObject bar;

    // Use this for initialization
    void Start()
    {
        cur_health = max_health;
    }
    void Update()
    {
      /*  if (cur_health <= 0)
        {
            Gameover();
        } */
    }

    // Update is called once per frame
    void OnTriggerEnter2D()
    {
        decreasehealth();
    }
    void decreasehealth()
    {
        cur_health -= 1f;
        float cal_health = cur_health / max_health;
        SetHealthBar(cal_health);
    }
    public void SetHealthBar(float Health)
    {
        bar.transform.localScale = new Vector3(Health, bar.transform.localScale.y, bar.transform.localScale.z);
    }
  /*  void Gameover()
    {
        Destroy(this.gameObject);
        SceneManager.LoadScene(0);
    } */
}
